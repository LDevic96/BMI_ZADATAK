package hr.ferit.ldevic.zadatakbmi;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    TextView tvHeight;
    EditText etEnterHeight;
    TextView tvWeight;
    EditText etEnterWeight;
    Button bResult;
    TextView tvResult;

    float height, weight;



        @Override
        protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
            initialize();
    }

    private void initialize() {
        this.tvHeight = (TextView) findViewById(R.id.tvHeight);
        this.etEnterHeight = (EditText) findViewById(R.id.etEnterHeight);
        this.tvWeight = (TextView) findViewById(R.id.tvWeight);
        this.etEnterWeight = (EditText) findViewById(R.id.etEnterWeight);
        this.bResult = (Button) findViewById(R.id.bResult);
        this.tvResult = (TextView) findViewById(R.id.tvResult);
    }

    public void Bmi(View view) {
        String height1 = etEnterHeight.getText().toString();
        String weight1 = etEnterWeight.getText().toString();

        float height = Float.parseFloat(height1);
        float weight = Float.parseFloat(weight1);

        float bmi;
        bmi = weight/(height*height);
        tvResult.setText(String.valueOf(bmi));


    }
}

